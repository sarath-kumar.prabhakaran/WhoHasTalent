package com.cbs.WhoHasTalent;

public class Vocalist extends Participants implements keyRange{
	
	public Vocalist() {
		super();
	}
	
	@Override
	public String perform(String key,int id) {
		return "I sing in the key of – "+ key +"-["+id+"]";
	}

	public String performOnvolume(String key,int volrange,int id) {
		return "I sing in the key of – "+key+"-"+"at the volume -"+ volrange +"-["+id+"]";
	}
}
