package com.cbs.WhoHasTalent;

public class Dancer extends Participants{
	
	public Dancer() {
		super();
	}
	
	public String getdancer() {
		return "dancer";
	}

	@Override
	public String perform(String style,int id) {
		return style+"-["+id+"]-"+getdancer();
	}

	

}