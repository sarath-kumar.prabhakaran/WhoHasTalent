package com.cbs.WhoHasTalent;

public class DefaultPerformer extends Participants{
	
	public DefaultPerformer() {
		super();
	}
	
	@Override
	public String perform(String common,int id) {
		return "["+id+"]-"+common;
	}
	
}
