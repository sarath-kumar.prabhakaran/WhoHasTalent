package com.cbs.WhoHasTalent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Audition {
	
	   private int id;
	   private String style;
	   private String key;
	   private int volrange;
	   private String act;
	   
	   public void setId(int id) {
			this.id = id;
		}

		public int getId() {
			return id; 
		}
		
	   
	   private Participants perf;
	   private List<Audition> emp= new ArrayList<Audition>();
	   
	   keyRange perfV = new Vocalist();
	   
	   public Audition(Participants PassengerType,int id) {
		   setPerf(PassengerType);
		   setId(id);
		   setAct();
	   }
	   
	   public Audition(String Style,Participants PassengerType,int id) {
		   setStyle(Style);
		   setPerf(PassengerType);
		   setId(id);
	   }
	   
	   public Audition(String key,int volrange,Participants PassengerType,int id) {
		   setKey(key);
		   setVolrange(volrange);
		   setPerf(PassengerType);
		   setId(id);
	   }
	   
	   public String defaultPerformer() {
		   return perf.perform(getAct(),id);
	   }
	   
	   public String defaultdancer() {
		   return perf.perform(getStyle(),id);
	   }
	  
	   public String defaultvocalist() {
		   return perf.perform(getKey(),id);
	   }
	   
	   public String rangeVocalist() {
		   return perfV.performOnvolume(getKey(),getVolrange(),id);
	   }
	   
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public int getVolrange() {
		return volrange;
	}
	public void setVolrange(int volrange) {
		this.volrange = volrange;
	}
	public List<Audition> getEmp() {
		return emp;
	}
	public void setEmp(List<Audition> emp) {
		this.emp = emp;
	}
	
	public Participants getPerf() {
		return perf;
	}

	public void setPerf(Participants perf) {
		this.perf = perf;
	}
	
	public String getAct() {
		return act;
	}

	public void setAct() {
		this.act = "performer";
	}
}
