package com.cbs.WhoHasTalent;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class WhoHasTalentTest extends TestCase {
	
	@Before
	public void Start() {
	}
	
	@Test
	public void testdancer() {
		
		Audition a = new Audition("pop", new Dancer(),35);
		a.defaultdancer();
		
		int actual = 35;
		int expected = a.getId();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testdancerstyle() {
		
		Audition a = new Audition("pop", new Dancer(),35);
		a.defaultdancer();
		
		String actual1 = "pop";
		String expected1 = a.getStyle();
		assertEquals(expected1, actual1);
	}
	
	@Test
	public void testvocal() {
		
		Audition b = new Audition("G", 1 , new Vocalist(),37);
		b.defaultvocalist();
		int actual = 37;
		int expected = b.getId();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testvocalkey() {
		
		Audition b = new Audition("G", 1 , new Vocalist(),37);
		b.defaultvocalist();
	
		String actual1 = "G";
		String expected1 = b.getKey();
		assertEquals(expected1, actual1);
	}
	
	@Test
	public void testvocalrangekey() {
		
		Audition b = new Audition("G", 1 , new Vocalist(),37);
		b.defaultvocalist();
	
		int actual2 = 1;
		int expected2 = b.getVolrange();
		assertEquals(expected2, actual2);

	}
	
	@Test
	public void testvocalrange() {
		
		Audition d = new Audition("F", 8 , new Vocalist(),19);
		d.rangeVocalist();
		int actual = 19;
		int expected = d.getId();
		assertEquals(expected, actual);
		
	}
	
	@Test
	public void testvocalrangek() {
		
		Audition d = new Audition("F", 8 , new Vocalist(),19);
		d.rangeVocalist();
		
		String actual1 = "F";
		String expected1 = d.getKey();
		assertEquals(expected1, actual1);

	}
	
	@Test
	public void testvocalranges() {
		
		Audition d = new Audition("F", 8 , new Vocalist(),19);
		d.rangeVocalist();

		int actual2 = 8;
		int expected2 = d.getVolrange();
		assertEquals(expected2, actual2);

	}
	
	@Test
	public void otherPerformers() {
		
		Audition c = new Audition(new DefaultPerformer(),98);
		c.defaultPerformer();
		int actual = 98;
		int expected = c.getId();
		assertEquals(expected, actual);

	}

	@Test
	public void testallperformerstest() {
		
		Audition p1 = new Audition(new DefaultPerformer(),1);
		String actual = "[1]-performer";
		String expected = p1.defaultPerformer();
		assertEquals(expected, actual);
		
		Audition p2 = new Audition(new DefaultPerformer(),2);
		String actual1 = "[2]-performer";
		String expected1 = p2.defaultPerformer();
		assertEquals(expected1, actual1);
		
		Audition p3 = new Audition(new DefaultPerformer(),3);
		String actual3 = "[3]-performer";
		String expected3 = p3.defaultPerformer();
		assertEquals(expected3, actual3);
		
		Audition p4 = new Audition(new DefaultPerformer(),4);
		String actual4 = "[4]-performer";
		String expected4 = p4.defaultPerformer();
		assertEquals(expected4, actual4);
		
		Audition d1 = new Audition("HipHop", new Dancer(),5);
		String actual5 = "HipHop-[5]-dancer";
		String expected5 = d1.defaultdancer();
		assertEquals(expected5, actual5);
		
		Audition d2 = new Audition("Jazz", new Dancer(),6);
		String actual6 = "Jazz-[6]-dancer";
		String expected6 = d2.defaultdancer();
		assertEquals(expected6, actual6);
		
		Audition v1 = new Audition("G", 9 , new Vocalist(),7);
		String actual7 = "I sing in the key of – G-[7]";
		String expected7 = v1.defaultvocalist();
		assertEquals(expected7, actual7);
		
		Audition v2 = new Audition("G", 9 , new Vocalist(),7);
		String actual8 = "I sing in the key of – G-at the volume -9-[7]";
		String expected8 = v2.rangeVocalist();
		assertEquals(expected8, actual8);
	
	}
	
	@After
	public void End() {
	}
	

}
